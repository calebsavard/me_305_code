'''!@file           closedloop_04.py
    @brief          Class for a proportional controller.
    @details        Class for a proportional controller containing an update() and set_Kp() methods.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           February 17, 2022
'''
import shares_04

class ClosedLoop_04:
    '''!@brief      Driver class for a Closed Loop Controller
        @details    Used to control a signal and provide output to achieve desired steady state
    
    '''

    def __init__(self, 
                input_signal: shares_04.Share_04, 
                target_val: shares_04.Share_04, 
                prop_gain: int, 
                sat_limit_low: int, 
                sat_limit_high: int):
        '''!@brief                  Initializes the ClosedLoop object.     
            @details                   Objects of this class act as a Proportional Controller, 
                                    taking in an input signal and target value, along with a proportional
                                    gain. 
            @param  input_signal    Share object containing the input signal to the controller.
            @param  target_val      Share object containing the desired reference value for the controller.
            @param  prop_gain       Share object containing the Kp for the controller.
            @param  sat_limit_low   Minimum value to saturate the controller output at.
            @param  sat_limit_high  Maximum value to saturate the controller output at.
            
        '''

        self._input_signal = input_signal
        self._target_val = target_val
        self._Kp = prop_gain
        self._sat_low = sat_limit_low
        self._sat_high = sat_limit_high

    def update(self):
        '''!@brief          Returns an output signal based on the initialized share variables
            @details        Multiplies the error signal by the proportional gain to obtain an output signal.
                            If the output signal exceeds the saturation limits, return the saturation limit instead.
        '''
        out_signal = self._Kp * (self._target_val.read() - self._input_signal.read())
        
        if out_signal > self._sat_high:
            out_signal = self._sat_high
        elif out_signal < self._sat_low:
            out_signal = self._sat_low
        
        return out_signal


    def set_Kp(self, new_Kp):
        '''!@brief          Sets the Kp to a new value
            @details        Replaces the existing Kp with a new value.
            @param  new_Kp  The new value of Kp (proportional gain) to be set.
            
        '''
        self._Kp = new_Kp   

