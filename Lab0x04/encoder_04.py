'''!@file       encoder_04.py
    @brief      Driver class for rotary encoders
    @details	For creating encoder objects, initializing them, and calling relevant methods.
    @author	    Chris Linthacum
    @author     Caleb Savard
    @date       February 2, 2022
'''

import pyb
import time
import math
from time import ticks_us, ticks_add, ticks_diff



class Encoder_04:
    '''!@brief      Class to represent encoder object to handle encoder functions
        @details    Class to represent an encoder connected to two pins on board. Pins and channels
                    may be specified through init arguments, but the user must confirm that the
                    desired Pin and channel combination are allowed by the hardware specifications.
                    <p>At very high speeds, the counter may fill too much between updates for this driver to handle.
                    The minimum update speed is given by</p>
                    <img src = "Resources/encoderSpeedEqn.gif" alt = "Equation minimum update frequency">
                    <p> where &omega; is the motor speed, CPR is the number of ticks per revolution, and &Delta; is the
                    rollover cutoff, 1/2 the 'period' from the init method.</p>
    '''

    def __init__(self, channel: int, pin1: pyb.Pin, pin2: pyb.Pin, sample_freq: int, period: int = 65535, prescaler: int = 1, flipVelDirection: bool = False):
        '''!@brief          Creates encoder object
            @details        You must ensure you are using a valid combination of pins and timer, found in the STM32 documentation.
            @param  channel Which channel is used on the timer of choice. This is often simply 1.
            @param  pin1    The first of the encoder's data pins.
            @param  pin2    The second of the encoder's data pins.
            @param  period  The maximum number of ticks before rollover. By default, this is set to the maximum 16-bit number.
            @param  prescaler   How many encoder ticks before the clock triggers. Increasing this can reduce rollover error at high speeds.
        '''
        print("Creating encoder object")
        ##  @brief      First encoder pin
        #   @details    Signal pin from the first of the two sensors on the encoder.
        self.pin_1 = pyb.Pin(pin1, pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pyb.Pin.AF2_TIM4)
        ##  @brief      Second encoder pin
        #   @details    Signal pin from the second of the two sensors on the encoder.
        self.pin_2 = pyb.Pin(pin2, pyb.Pin.AF_PP, pull=pyb.Pin.PULL_NONE, af=pyb.Pin.AF2_TIM4)

        ##  @brief      Timer object
        #   @details    Holds the timer setup for the timer used in the encoder
        self.encoder_timer = pyb.Timer(4, prescaler=prescaler, period=period)
        ##  @brief      Channel object
        #   @details    Holds the channel setup for the timer used in the encoder.
        self.encoder_channel = self.encoder_timer.channel(channel, pyb.Timer.ENC_AB)

        ##  @brief      Cutoff for detecting rollover
        #   @details    Set to 1/2 the period.
        self.delta_cutoff = math.floor(period/2)
        ##  @brief      Encoder counter max.
        #   @details    The maximum number of ticks before rollover.
        self.period = period

        ##  @brief      Encoder position
        #   @details    Stores the position of the encoder (in ticks)
        self.abs_position = 0
        ##  @brief      Last encoder position
        #   @details    Position of the encoder (in ticks) from the last update
        self.last_pos = 0
        ##  @brief      Change in position
        #   @details    Difference (in ticks) since the last update
        self.prev_delta = 0
        
        self._sample_freq = sample_freq

        self._last_delta_time = ticks_us()
        self._delta_t = 1

        if flipVelDirection:
            self._direction_const = -1
        else:
            self._direction_const = 1



    def update(self):
        '''!@brief      Saves encoder value.
            @details    Saves the current position of the encoder (in ticks) to self.abs_position. Will handle rollover in 
                        direction unless the encoder is spinning too quickly (see class description for details)
        '''
        # print("Reading encoder count and updating position and delta values")
        delta = self.encoder_timer.counter() - self.last_pos
        if delta < -(self.delta_cutoff):
            delta = (self.encoder_timer.counter() + self.period) - self.last_pos
        elif delta > self.delta_cutoff:
            delta = (self.encoder_timer.counter() - self.period) - self.last_pos

        # print("Encoder Val: ", self.encoder_timer.counter(), " Last Pos: ", self.last_pos)
        # print("Final Delta: ", delta)
        self.abs_position += delta
        self.last_pos = self.encoder_timer.counter()
        self.prev_delta = delta

        current_time = ticks_us()
        self._delta_t = ticks_diff(current_time, self._last_delta_time)
        # print(self._delta_t)
        self._last_delta_time = current_time


    def get_position(self):
        '''!@brief      Returns position in radians.
            @details    Returns currently saved position of the encoder (in ticks) from self.abs_position.
        '''

        return 2 * math.pi * self.abs_position / 4000

    def get_velocity(self):
        '''!@brief      Returns angular velocity.
            @details    Returns current angular velocity from the saved delta.
        '''
        # 2pi * delta / num delta per rev (4000)
        
        return self._direction_const * (2 * math.pi * self.prev_delta / 4000) / (self._delta_t / 10**6)


    def zero(self, position=0):
        '''!@brief      Zeros the encoder.
            @details    Set the current absolute position to the value passed in; zero by default.
            @param  position    The current absolute position is set to this value.
        '''

        print("Setting position back to zero")
        self.abs_position = position


    def get_delta(self):
        '''!@brief      Returns the latest saved difference in position.
            @details    Getting the delta can act as a velocity measurement when combined with the sample frequency.
        '''
        return self.prev_delta


# We don't need this part anymore, right?
if __name__ == "__main__":
    print("Updating encoder values and printing every 1 second:")
    
