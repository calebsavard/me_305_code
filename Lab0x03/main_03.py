'''!@file               main_03.py
    @brief              Main function to cycle through tasks and run them
    @details            Implements a more advanced user interface and incorporates motor control.

    @author             Caleb Savard
    @author             Chris Linthacum
    @date               February 17, 2022
'''
import shares_03
import taskUser_03
import taskEncoder_03
import taskMotor_03

def main():
    '''!@brief      Main driver executing taskUser.taskUserFcn() and taskEncoder.taskEncoderFcn()
        @details    The main driver function for Lab0x02. Cycles through 2 task management functions
                    (taskUser.taskUserFcn() and taskEncoder.taskEncoderFcn()) that manage states for the User
                    Interface and encoder management tasks. Terminates on KeyboardInterrupt.
        @returns    None
    '''
    zFlag = shares_03.Share_03(False)
    pFlag = shares_03.Share_03(False)
    dFlag = shares_03.Share_03(False)
    vFlag = shares_03.Share_03(False)
    gFlag = shares_03.Share_03(False)
    sFlag = shares_03.Share_03(False)
    duty1 = shares_03.Share_03(None)
    duty2 = shares_03.Share_03(None)
    dataPrint = shares_03.Share_03(None)
    cFlag = shares_03.Share_03(False)
    tFlag = shares_03.Share_03(False)
    testData = shares_03.Share_03(False)


    sample_period = 100000
    
    task_list = [taskUser_03.taskUserFcn_03("Task User", 10_000, zFlag, pFlag, dFlag, vFlag, duty1, duty2, cFlag, gFlag, tFlag, sFlag, dataPrint, testData), 
                 taskEncoder_03.taskEncoderFcn_03("Task Encoder", 10_000, sample_period, zFlag, pFlag, dFlag, gFlag, sFlag, dataPrint, testData, vFlag),
                 taskMotor_03.taskMotorFcn_03("Task Motor", 10_000, duty1, duty2, cFlag)]

    while True:
        try:
            for task in task_list:
                next(task)
        except KeyboardInterrupt:
            break
    
    print("Terminating Program")



if __name__ == '__main__':
    main()