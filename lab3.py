'''!@page lab_3         Lab 3
    @brief              This page contains information about the Lab 3 assignment.

    @section fsm        Finite State Machine Dagrams
                        <img src="Resources/Lab3_State_taskEncoder.PNG" alt="Lab 3 taskEncoder diagram" width="600"><br/> 
                        <img src="Resources/Lab3_State_taskMotor.PNG" alt="Lab 3 taskMotor diagram" width="600"><br/> 
                        <img src="Resources/Lab3_State_taskUser.PNG" alt="Lab 3 taskUser diagram" width="600"><br/> 

    @section task_dia   Task Diagrams
                        <img src="Resources/Lab3_taskDiagram.PNG" alt="Lab 3 task diagram" width="600"><br/> 

    @section plot1      Plot of Encoder Position vs Time
                        By pressing 'g' on the keyboard, we recorded 30 seconds of encoder position data. During the 30 second collection
                        period, we varied the Motor 1 duty cycle. An output of the encoder position vs. time is shown here.<br/> 
                        <img src="Resources/lab3_encoder_vs_time.png" alt="encoder position vs time" width="500"><br/> 

    @section mot_sped   Motor Speed at Varying Duty Cycles
                        Using our user interface, we recorded the motor speed in radians/second at various duty cycle values. We noted
                        that the direction of the encoder is flipped from our motor direction, but this could be remedied through software
                        or through reversing the leads on the hardware, and we decided to leave it untouched for now as to not cause 
                        unforseen complications with later development.<br/> 
                        <img src="Resources/mot_speed_vs_duty_cycle.png" alt="speeds of encoder at various duty cycles" width="500"><br/> 

    @author             Chris Linthacum
    @author             Caleb Savard

    @copyright          Copyright 2021 Chris Linthacum and Caleb Savard

    @date               February 17, 2022
'''
                       