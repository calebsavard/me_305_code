'''!@page homework_2    Homework 2
    @brief              This page contains the hand calculations for the Homework 0x02 assigment for ME 305.
                       

    @section part_1     1. Simplified Schematic
                        <img src="Resources/hw2_assn1.png" alt="homework 2 1-1" width="800"><br/>
                        <img src="Resources/hw2_1-1.png" alt="homework 2 1-1" width="800"><br/>
                        <img src="Resources/hw2_1-2.png" alt="homework 2 1-1" width="800"><br/>
                        
    @section part_2     2. Kinematics
                        <img src="Resources/hw2_assn2.png" alt="homework 2 1-1" width="800"><br/>
                        <img src="Resources/hw2_2-1.png" alt="homework 2 2-1" width="800"><br/>
                        <img src="Resources/hw2_2-2.png" alt="homework 2 2-2" width="800"><br/>
                        <img src="Resources/hw2_2-3.png" alt="homework 2 2-3" width="800"><br/>

    @section part_3     3. Relationship Between Torque and Moment
                        <img src="Resources/hw2_assn3.png" alt="homework 2 1-1" width="800"><br/>
                        <img src="Resources/hw2_3-1.png" alt="homework 2 3-1" width="800"><br/>

    @section part_4     4. Equations of Motion
                        <img src="Resources/hw2_assn4.png" alt="homework 2 1-1" width="800"><br/>
                        <img src="Resources/hw2_4-1.png" alt="homework 2 4-1" width="800"><br/>
                        <img src="Resources/hw2_4-2.png" alt="homework 2 4-2" width="800"><br/>
                        <img src="Resources/hw2_4-3.png" alt="homework 2 4-3" width="800"><br/>

    @section part_5     5. Rearrange Equations
                        <img src="Resources/hw2_assn5.png" alt="homework 2 1-1" width="800"><br/>
                        <img src="Resources/hw2_5-1.png" alt="homework 2 5-1" width="800"><br/>
                        <img src="Resources/hw2_5-2.png" alt="homework 2 5-2" width="800"><br/>

    @section part_6     6. Manipulate to Matrix Form
                        <img src="Resources/hw2_assn6.png" alt="homework 2 1-1" width="800"><br/>
                        <img src="Resources/hw2_6-1.png" alt="homework 2 6-1" width="800"><br/>

    @author             Chris Linthacum
    @author             Caleb Savard

    @copyright          Copyright 2021 Chris Linthacum and Caleb Savard

    @date               February 13, 2022
'''
