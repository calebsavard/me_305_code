'''!@page lab_4         Lab 4
    @brief              This page contains information about the Lab 4 assignment.

    @section fsm        Finite State Machine Dagrams
                        <img src="Resources/Lab4TaskEncoderFcnDiagram.png" alt="Lab 4 taskEncoder diagram" width="600"><br/> 
                        <img src="Resources/Lab4TaskMotorFcnDiagram.png" alt="Lab 4 taskMotor diagram" width="600"><br/> 
                        <img src="Resources/Lab4TaskUserFcnDiagram.png" alt="Lab 4 taskUser diagram" width="600"><br/> 
                        <img src="Resources/Lab4TaskControllerFcnDiagram.png" alt="Lab 4 taskUser diagram" width="600"><br/> 

    @section task_dia   Task Diagram
                        <img src="Resources/Lab4TaskDiagram.png" alt="Lab 3 task diagram" width="600"><br/> 

    @section plot1      Plots of Tuning

	\htmlinclude Lab4Report.html

    @section block_dia  Block Diagram of Closed Loop Controller
                        Here is a block diagram of the closed loop control system. Given a target velocity, the controller will use proportional control
                        to try and achieve the signal.<br/> 
                        <img src="Resources/Lab4ClosedLoopBlockDiagram.png" alt="closed loop controller block diagram" width="1000"><br/> 

    @author             Chris Linthacum
    @author             Caleb Savard

    @copyright          Copyright 2021 Chris Linthacum and Caleb Savard

    @date               February 23, 2022
'''
                       
