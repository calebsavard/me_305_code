'''!@file           taskController_05.py
    @brief          Sets up and calls the closed loop controller class.
    @details        Configures a proportional controller object and continuously updates the controller output value and assigns to the proper motor duty cycle.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           February 17, 2022
'''

import shares_05
from time import ticks_us, ticks_diff, ticks_add
import closedloop_05

def taskControllerFcn_05(taskName: str, 
                        period: int, 
                        euler_angles: shares_05.Share_05,
                        angular_velocities: shares_05.Share_05,
                        roll_target_pos: shares_05.Share_05,
                        roll_target_deriv: shares_05.Share_05,
                        pitch_target_pos: shares_05.Share_05,
                        pitch_target_deriv: shares_05.Share_05,
                        prop_gain: shares_05.Share_05, 
                        deriv_gain: shares_05.Share_05,
                        cont_enable: shares_05.Share_05, 
                        roll_target_duty: shares_05.Share_05,
                        pitch_target_duty: shares_05.Share_05):
    '''!@brief      Sets up and calls the closed loop controller class.
        @details    Every period, this task resets the Kp using set_Kp and updates the controller. Saturation limits are set to 100 and -100.
        @param  taskName        Task name label for debugging purposes.
        @param  period          Period to run taskControllerFcn
        @param  input_signal    Shared variable to store controller input signal. In this case the actual encoder velocity.
        @param  target_val      Shared variable to store the target value for encoder velocity.
        @param  prop_gain       Shared variable to store the controller proportional gain value.
        @param  cont_enable     Shared variable to store a boolean whether to enable or disable the controller.
        @param  target_duty     Shared variable to point to the duty cycle to be affected by the controller.
    '''
    
    try:
        roll_position_signal = shares_05.Share_05(euler_angles.read()['EUL_ROLL'])
        pitch_position_signal = shares_05.Share_05(euler_angles.read()['EUL_PITCH'])

        roll_deriv_signal = shares_05.Share_05(angular_velocities.read()['GYR_DATA_X'])
        pitch_deriv_signal = shares_05.Share_05(angular_velocities.read()['GYR_DATA_Y'])
    
    except TypeError:
        roll_position_signal = shares_05.Share_05(0)
        pitch_position_signal = shares_05.Share_05(0)

        roll_deriv_signal = shares_05.Share_05(0)
        pitch_deriv_signal = shares_05.Share_05(0)


    
    roll_controller = closedloop_05.ClosedLoop_05(roll_position_signal, roll_deriv_signal, roll_target_pos, roll_target_deriv, prop_gain.read(), deriv_gain.read(), sat_limit_high=70, sat_limit_low=-70)
    pitch_controller = closedloop_05.ClosedLoop_05(pitch_position_signal, pitch_deriv_signal, pitch_target_pos, pitch_target_deriv, prop_gain.read(), deriv_gain.read(), sat_limit_high=70, sat_limit_low=-70)

    next_time = ticks_add(ticks_us(), period) 
    
    while True:
        # Initialize the timing system
        current_time = ticks_us()
        
        if cont_enable.read():
            
            # When it's time...
            if ticks_diff(current_time, next_time) >= 0:
                # print("Running controller update")
            
                next_time = ticks_add(next_time, period)

                # Update values for position and deriv signals
                # print(euler_angles.read())

                roll_position_signal.write(euler_angles.read()['EUL_ROLL'])
                pitch_position_signal.write(euler_angles.read()['EUL_PITCH'])

                roll_deriv_signal.write(angular_velocities.read()['GYR_DATA_X'])
                pitch_deriv_signal.write(angular_velocities.read()['GYR_DATA_Y'])
                
                # Update values for roll_controller
                roll_controller.set_Kp(prop_gain.read())
                roll_controller.set_Kd(deriv_gain.read())
                new_duty = roll_controller.update()
                roll_target_duty.write(new_duty)
                # print(f"Roll duty = {new_duty}")

                # Update values for pitch_controller
                pitch_controller.set_Kp(prop_gain.read())
                pitch_controller.set_Kd(deriv_gain.read())
                new_duty = pitch_controller.update()
                pitch_target_duty.write(new_duty)
                # print(f"Pitch duty = {new_duty}")

                # print(f"Motor Y Duty = {pitch_target_duty.read()}")
                # print(f"Motor X Duty = {roll_target_duty.read()}")
                
        yield None
