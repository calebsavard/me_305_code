'''!@file           taskIMU_05.py
    @brief          Creates and operates the IMU.
    @details        Instantiates an I2C device for and IMU (BNO055 in particular). 
                    The pyBoard is set as the controller by default and the IMU is
                    set to NDOF mode by default.
                    
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           March 2, 2022
'''

# Import stuff
import BNO055_05
from printqueue_05 import PrintQueue, PrintJob
import pyb
from time import ticks_us, ticks_add, ticks_diff
import micropython
import os
import shares_05

# State names
S0_INIT = micropython.const(0)
S1_CALIB = micropython.const(1)
S2_RUN = micropython.const(2)


def taskIMUFcn(taskName: str,
               period: int,
               euler_angles: shares_05.Share_05,
               angular_velocities: shares_05.Share_05,
               calib_status: shares_05.Share_05,
               print_queue: PrintQueue):
    '''!@brief              Task for operating the IMU
        @details            Finite state machine that controls IMU creation, calibration, and operation.
        @param  taskName    Brief string to describe the instance of the function. Useful for debug purposes.
        @param  period      Period with which to run the function and update/execute state logic.
    '''

    # Initialize, create object
    # print("Creating IMU object")
    IMU1 = BNO055_05.BNO055_05(pyb.I2C(1, pyb.I2C.CONTROLLER), 0x28)
    NDOF_MODE = micropython.const(0b1100)
    # print('Entering NDOF mode')
    IMU1.change_op_mode(NDOF_MODE)

    state = 0
    statuses = None

    # Start timestamp system
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    while True:
        current_time = ticks_us()

        if ticks_diff(current_time, next_time) >= 0:

            next_time = ticks_add(next_time, period)

            if state == S0_INIT:

                state = S1_CALIB

                # Need to read angles to avoid NoneType error
                euler_angles.write(IMU1.read_euler_angles())
                angular_velocities.write(IMU1.read_angular_velocity())

                # Set calibration status to false by defauly
                calib_status.write(False)

            elif state == S1_CALIB:

                # If calibration file exists, read it and assign it.
                files = os.listdir()
                if "IMU_cal_coeffs.txt" in files:

                    print_queue.add_job(PrintJob('IMU Calibration file found'))
                    with open("IMU_cal_coeffs.txt", 'r') as f:

                        cal_data_string = f.readline()

                        cal_values = [
                            cal_value for cal_value in cal_data_string.strip().split(',')]

                        # print(IMU1.read_calib_coeff())

                        # raise ValueError("Intentional Stop")

                        # [Call the IMU object to write these values to the right directories]

                    calib_status.write(True)
                    print_queue.add_job(PrintJob('Loaded calibration values.'))

                    # Since we are calibrated change state
                    state = S2_RUN

                # If there's no calibration file manually calibrate
                else:
                    # If it's the first time, print calibration message and read coefficients.
                    if not statuses:

                        print_queue.add_job(
                            PrintJob('Calibration file not found, calibrating...'))
                        statuses = IMU1.calibration_status()

                    # If it's not the first time, update statuses unless calibration is complete.
                    elif (statuses['SYS_calib_status'] != 3) or \
                            (statuses['GYR_calib_status'] != 3) or \
                            (statuses['ACC_calib_status'] != 3) or \
                            (statuses['MAG_calib_status'] != 3):

                        statuses = IMU1.calibration_status()
                        print_queue.add_job(PrintJob(
                            f"SYS: {statuses['SYS_calib_status']}, GYRO: {statuses['GYR_calib_status']}, ACC: {statuses['ACC_calib_status']}, MAG: {statuses['MAG_calib_status']}", 20_000))

                    # Otherwise (if it's done), write calibration constants and clear statuses
                    else:
                        print_queue.add_job(PrintJob('Done Calibrating'))
                        print_queue.add_job(PrintJob(
                            f"SYS: {statuses['SYS_calib_status']}, GYRO: {statuses['GYR_calib_status']}, ACC: {statuses['ACC_calib_status']}, MAG: {statuses['MAG_calib_status']}"))

                        with open("IMU_cal_coeffs.txt", 'w') as f:

                            (ACC_OFFSET_X_LSB, ACC_OFFSET_X_MSB, ACC_OFFSET_Y_LSB, ACC_OFFSET_Y_MSB, ACC_OFFSET_Z_LSB, ACC_OFFSET_Z_MSB,
                             MAG_OFFSET_X_LSB, MAG_OFFSET_X_MSB, MAG_OFFSET_Y_LSB, MAG_OFFSET_Y_MSB, MAG_OFFSET_Z_LSB, MAG_OFFSET_Z_MSB,
                             GYR_OFFSET_X_LSB, GYR_OFFSET_X_MSB, GYR_OFFSET_Y_LSB, GYR_OFFSET_Y_MSB, GYR_OFFSET_Z_LSB, GYR_OFFSET_Z_MSB,
                             ACC_RADIUS_LSB, ACC_RADIUS_MSB, MAG_RADIUS_LSB, MAG_RADIUS_MSB) = IMU1.read_calib_coeff()

                            f.write(f"{ACC_OFFSET_X_LSB}, {ACC_OFFSET_X_MSB}, {ACC_OFFSET_Y_LSB}, {ACC_OFFSET_Y_MSB}, {ACC_OFFSET_Z_LSB}, {ACC_OFFSET_Z_MSB}, \
                                    {MAG_OFFSET_X_LSB}, {MAG_OFFSET_X_MSB}, {MAG_OFFSET_Y_LSB}, {MAG_OFFSET_Y_MSB}, {MAG_OFFSET_Z_LSB}, {MAG_OFFSET_Z_MSB}, \
                                    {GYR_OFFSET_X_LSB}, {GYR_OFFSET_X_MSB}, {GYR_OFFSET_Y_LSB}, {GYR_OFFSET_Y_MSB}, {GYR_OFFSET_Z_LSB}, {GYR_OFFSET_Z_MSB}, \
                                    {ACC_RADIUS_LSB}, {ACC_RADIUS_MSB}, {MAG_RADIUS_LSB}, {MAG_RADIUS_MSB}\r\n")

                        statuses = None

                        print_queue.add_job(PrintJob('Calibration Complete.'))

                        calib_status.write(True)

                        # raise ValueError("Calibs written")

                        state = S2_RUN

            elif state == S2_RUN:

                # Always write the updated euler angles to the shared variable
                euler_angles.write(IMU1.read_euler_angles())

                # print(euler_angles.read())

                # if pFlag.read() == True:
                #     pFlag.storeData(IMU1.read_angles())

                #     pFlag.write(False)

                # Always write the updated angular velocities to shared variable
                angular_velocities.write(IMU1.read_angular_velocity())

                # print(angular_velocities.read())

                # # If vFlag is true, read the angular velocities and write them to vFlag
                # if vFlag.read() == True:
                #     vFlag.storeData(IMU1.read_velocies())

                #     vFlag.write(False)

            # Error if there's a problem with the state machine
            else:

                raise ValueError('Invalid State!')

            yield state


def testPrint(period: int, euler_angles: shares_05.Share_05, angular_velocities: shares_05.Share_05, calib_status: shares_05.Share_05):

    # Start timestamp system
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    while True:
        current_time = ticks_us()

        if ticks_diff(current_time, next_time) >= 0:

            next_time = ticks_add(next_time, period)

            if calib_status.read():

                angles = euler_angles.read()
                print("Euler angles:")
                print(
                    f"   Heading: {angles['EUL_HEADING']}, Roll: {angles['EUL_ROLL']}, Pitch: {angles['EUL_PITCH']}")
                velocities = angular_velocities.read()
                print("Angular Velocities:")
                print(
                    f"   Gyro X: {velocities['GYR_DATA_X']}, Gyro Y: {velocities['GYR_DATA_Y']}, Gyro Z: {velocities['GYR_DATA_Z']}")

        yield


if __name__ == '__main__':

    euler_angles = shares_05.Share_05(None)
    angular_velocities = shares_05.Share_05(None)
    calib_status = shares_05.Share_05(False)

    task_list = [taskIMUFcn('TaskIMU', 10_000, euler_angles, angular_velocities, calib_status),
                 testPrint(1_000_000, euler_angles, angular_velocities, calib_status)]
    while True:
        try:
            for task in task_list:
                next(task)
        except KeyboardInterrupt:
            break

    print("Terminating Program")
