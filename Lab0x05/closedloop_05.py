'''!@file           closedloop_05.py
    @brief          Class for a proportional controller.
    @details        Class for a proportional controller containing an update() and set_Kp() methods.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           February 17, 2022
'''
import shares_05

class ClosedLoop_05:
    '''!@brief      Driver class for a Closed Loop Controller
        @details    Used to control a signal and provide output to achieve desired steady state
    
    '''

    def __init__(self, 
                position_signal: shares_05.Share, 
                deriv_signal: shares_05.Share,
                target_pos: shares_05.Share, 
                target_deriv: shares_05.Share,
                prop_gain: float,
                deriv_gain: float,
                sat_limit_low: int, 
                sat_limit_high: int):
        '''!@brief                  Initializes the ClosedLoop object.     
            @details                   Objects of this class act as a Proportional Controller, 
                                    taking in an input signal and target value, along with a proportional
                                    gain. 
            @param  position_signal    Share object containing the position input signal to the controller.
            @param  deriv_signal    Share object containing the input derivative signal for the controller.
            @param  target_pos      Share object containing the desired reference value for the controller position.
            @param  target_deriv    Share object containing the desired reference value for controller velocity.
            @param  prop_gain       Float value for the Kp for the controller.
            @param  deriv_gain      Float value for the Kd for the controller
            @param  sat_limit_low   Minimum value to saturate the controller output at.
            @param  sat_limit_high  Maximum value to saturate the controller output at.
            
        '''

        self._position_signal = position_signal
        self._deriv_signal = deriv_signal
        self._target_pos = target_pos
        self._target_deriv = target_deriv
        self._Kp = prop_gain

        self._Kd = deriv_gain
        self._sat_low = sat_limit_low
        self._sat_high = sat_limit_high

    def update(self):
        '''!@brief          Returns an output signal based on the initialized share variables
            @details        Multiplies the error signal by the proportional gain and adds the velocity/derivitive error signal to obtain an output signal.
                            If the output signal exceeds the saturation limits, return the saturation limit instead.
        '''
        out_signal = self._Kp * (self._target_pos.read() - self._position_signal.read()) + self._Kd * (self._target_deriv.read() - self._deriv_signal.read())
        # out_signal = self._Kp * (self._target_pos.read() - self._position_signal.read())
        # print(
            # f"Out Signal: {out_signal}\nPosition Error: {(self._target_pos.read() - self._position_signal.read())}\n Derivative Error: {(self._target_deriv.read() - self._deriv_signal.read())}")
        if out_signal > self._sat_high:
            out_signal = self._sat_high
        elif out_signal < self._sat_low:
            out_signal = self._sat_low

        # print(f"Position Signal: {self._position_signal.read()}")

        # print(f"Out signal: {out_signal}")
        
        return out_signal


    def set_Kp(self, new_Kp):
        '''!@brief          Sets the Kp to a new value
            @details        Replaces the existing Kp with a new value.
            @param  new_Kp  The new value of Kp (proportional gain) to be set.
            
        '''
        self._Kp = new_Kp   

    def set_Kd(self, new_Kd):
        '''!@brief          Sets the Kd to a new value
            @details        Replaces the existing Kd with a new value.
            @param  new_Kd  The new value of Kd (derivative gain) to be set.
            
        '''

        self._Kd = new_Kd
