'''!@file           taskUser_05.py
    @brief          Manages the states for the User Interface task.
    @details        Manages the current state and executes state-related logic for 
                    all User Interface functionality. Cycles through states for processing
                    input, zeroing encoder position, and printing output.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           February 2, 2022
'''

from time import ticks_us, ticks_add, ticks_diff
import array
from printqueue import PrintJob, PrintQueue
from pyb import USB_VCP
import micropython
import shares
import math

## Maps state 0 to a more readable name
S0_INIT = micropython.const(0)
## Maps state 1 to a more readable name
S1_CMD = micropython.const(1)
## Maps state 2 to a more readable name
S2_ZERO = micropython.const(2)
## Maps state 3 to a more readable name
S3_PRINT = micropython.const(3)
## Maps state 4 to a more readable name
S4_INPUT = micropython.const(4)
## Maps state 5 to a more readable name
S5_TEST = micropython.const(5)
## Maps state 6 to a more readable name
S6_SRESP = micropython.const(6)


def printLegend():
    '''!
    @brief          Prints the available user key commands to the console
    @details        Prints a table of available user input commands to the console to
                    preview the available user key commands.
    '''
    print("|------------------------------------------------------------|")
    print("|                   Available User Commands                  |")
    print("|------------------------------------------------------------|")
    print("| p or P | Print Euler angles (in degrees) from IMU          |")
    print("| v or V | Print out the angular velocities from IMU         |")
    print("| s or S | End data collection prematurely                   |")
    print("| y      | Choose a Pitch set point for closed-loop control  |")
    print("| Y      | Choose a Roll set point for closed-loop control   |")
    print("| k      | Enter new proportional gain (Kp)                  |")
    print("| K      | Enter new derivative gain (Kd)                    |")
    print("| w or W | Enable or disable closed-loop control             |")
    print("| r or R | Perform an initial value response, from initial   |")
    print("|          conditions to platform back to 0,0                |")
    print("| x or X | Set both motor duty cycles to 0                   |")
    print("|------------------------------------------------------------|")
    print("\n")


# def printTestLegend():
#     '''!@brief          Prints the available user key commands in test interface to the console
#         @details        Prints a table of available user input commands to the console to
#                         preview the available user key commands in the testing interface.
#     '''
#     print("|------------------------------------------------------------|")
#     print("|                   Available User Commands                  |")
#     print("|------------------------------------------------------------|")
#     print("| s or S | Exit the testing interface and return to main     |")
#     print("|        | Also prints table of duty cycles and velocities   |")
#     print("|------------------------------------------------------------|")
#     print("\n")


def taskUserFcn(task_name: str,
                period: int,
                roll_target_duty: shares.Share,
                pitch_target_duty: shares.Share,
                gFlag: shares.Share,
                sFlag: shares.Share,
                prop_gain: shares.Share,
                deriv_gain: shares.Share,
                cont_enable: shares.Share,
                euler_angles: shares.Share,
                angular_velocities: shares.Share,
                roll_target_pos: shares.Share,
                pitch_target_pos: shares.Share,
                print_queue: PrintQueue):
    '''!@brief              Main task function to control UI states.
        @details            Manage different User Input states, including Init, Read_Cmd, Zero_Encoder, and Print_Data.
                            On function run, executes the logic of the current state and if appropriate shifts 
                            state for next run of function.
        @param task_name    Task name for the function to help with debugging
        @param period       Period to run execute function at. Period defines frequency that states are executed and refreshed.
        @param zFlag        Shared data object to encapsulate the z key being pressed. Signals that encoder should be zeroed. 
        @param pFlag        Shared data object to encapsulate the p key being pressed. Signals to print the encoder position.
        @param dFlag        Shared data object to encapsulate the d key being pressed. Signals to print the encoder delta.
        @param vFlag        Shared data object to encapsulate the v key being pressed. Signals to print the encoder velocity.
        @param roll_target_duty        Shared data object to store the duty cycle for motor 1.
        @param pitch_target_duty        Shared data object to store the duty cycle for motor 2.
        @param cFlag        Shared data object to encapsulate the c key being pressed. Signals to clear a motor fault.
        @param gFlag        Shared data object to encapsulate the g key being pressed. Signals to begin data collection.
        @param tFlag        Shared data object to encapsulate the t key being pressed. Starts the testing interface.
        @param sFlag        Shared data object to encapsulate the s key being pressed. Signals to end data collection prematurely.
        @param dataPrint    Shared data object to store the recorded encoder data. Stores both the position data and time values.
        @param testData     Shared data object to act as a flag to record avg velocity and to store the velocity data during testing.
        @param target_val   Shared data object to hold the target velocity value
        @param prop_gain    Shared data object to hold the proportional gain for the controllerTask
        @param cont_enable  Shared data object to specificy whether closed-loop controller is enabled (True) or not
        @param input_signal Shared data object to store the encoder velocity
        @yield state        If task ran, yields state the function is now in. If task did not evaluate, returns None.
    '''

    state = 0
    index_to_print = 0

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    serial_port = USB_VCP()

    return_state = S1_CMD
    s5_first_run = False
    s5_end_sig = False
    prompt_needed = True
    expecting_data = False

    new_duty = False

    buff = None

    printList = []

    setGain = False
    setDGain = False
    step_start_time = None
    temp_duty_cycle = None
    step_sample_period = 10_000
    step_sample_next = 0
    end_step_premature = False

    test_roll_data = None
    test_pitch_data = None
    sample_number = 0

    while True:

        current_time = ticks_us()

        if (ticks_diff(current_time, next_time) >= 0):

            next_time = ticks_add(next_time, period)

            if state == S0_INIT:

                # Print out the introduction information and the legend here
                print("Welcome to the program.\n")
                printLegend()
                state = S1_CMD

            elif state == S1_CMD:

                # Here we continously check for user input. We also process the user
                # input checking for values and setting the appropriate flags

                if print_queue.num_in() > 0:
                    state = S3_PRINT

                # Check for user input
                if serial_port.any():
                    read_char = serial_port.read(1).decode()

                    if read_char in ['p', 'P']:
                        # Set print out Euler angles
                        print("Print IMU Position (in degrees):")
                        angles = euler_angles.read()
                        print(f" heading = {angles['EUL_HEADING']}")
                        print(f"    roll = {angles['EUL_ROLL']}")
                        print(f"   pitch = {angles['EUL_PITCH']}")

                    elif read_char in ['v', 'V']:
                        # Print our angular velocities
                        print("Print Angular Velocities (in degrees/second):")
                        velocities = angular_velocities.read()
                        print(f" X Acc = {velocities['GYR_DATA_X']}")
                        print(f" Y Acc = {velocities['GYR_DATA_Y']}")
                        print(f" Z Acc = {velocities['GYR_DATA_Z']}")

                    elif read_char in ['s', 'S']:
                        # End data collection prematurely
                        if gFlag.read():
                            print("Stopping data collection...")
                            sFlag.write(True)

                    elif read_char in ['x', 'X']:
                        # Kill the motors
                        roll_target_duty.write(0)
                        pitch_target_duty.write(0)
                        print("Motors Off.")

                    elif read_char == 'm':
                        # Switch state to input duty cycle
                        # Also set inputTar (m1Flag) True to know where to write (motor 1)
                        print("Enter a duty cycle for motor 1:")
                        inputTar = roll_target_duty
                        buff = ""
                        state = S4_INPUT
                        return_state = S1_CMD

                    elif read_char == 'M':
                        # Switch state to input duty cycle
                        # Also set inputTar (m2Flag) True to know where to write (motor 2)
                        print("Enter a duty cycle for motor 2:")
                        inputTar = pitch_target_duty
                        buff = ""
                        state = S4_INPUT
                        return_state = S1_CMD

                    elif read_char in ['y']:
                        # Choose a Pitch set point for closed-loop control
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = pitch_target_pos
                        print("Enter a set point for pitch position:")

                    elif read_char in ['Y']:
                        # Choose a roll set point for closed-loop control
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = roll_target_pos
                        print("Enter a set point for roll position:")

                    elif read_char in ['k']:
                        # Set a new Kp for the controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = prop_gain
                        print("Enter a proportional gain (Kp) for the controller:")

                    elif read_char in ['K']:
                        # Set a new Kd for the controller
                        state = S4_INPUT
                        return_state = S1_CMD
                        buff = ""
                        inputTar = deriv_gain
                        print("Enter a derivative gain (Kd) for the controller:")

                    elif read_char in ['w', 'W']:
                        # Toggle whether the controller is enabled or disabled
                        cont_enable.write(not (cont_enable.read()))
                        print(f"Controller Enabled = {cont_enable.read()}")

                    elif read_char in ['r', 'R']:
                        # Go into the step response state
                        state = S6_SRESP
                        setGain = False
                        setDGain = False

                    else:
                        print(f"Invalid character '{read_char}' entered\n")
                        # Clear the input buffer
                        serial_port.read()
                        printLegend()

            # elif state == S2_ZERO:
            #     # Wait for zFlag to be set to false to return to state 1
            #     if not zFlag.read():
            #         print("Done.")
            #         state = S1_CMD

            elif state == S3_PRINT:
                # Iterate through printing until reached the end
                # For this state, print a list of lists, each list will be printed in its own column

                if print_queue.num_in() == 0:
                    state = S1_CMD
                else:
                    print(print_queue.get_job())

            elif state == S4_INPUT:
                # This is where we read in multiple values to get a float value. buff is defined as
                # None when the state is changed to S4_INPUT. That way, whenever the input state is
                # called, buff is empty. I don't think there's an instance where the input state could
                # be called twice in a row without exiting the input state? --Caleb

                # Check if there is a character, and read it as a string.
                if serial_port.any():
                    char = serial_port.read(1).decode()

                    if char.isdigit():         # Check if it's a digit and add it.
                        buff += char
                    # Check if it's a minus at the beginning and add it.
                    elif char == '-' and buff == "":
                        buff += char
                    # Check if it's a backspace and NOT the first character (better method?)
                    elif char in ['\b', '\0x08', '\x7F'] and not buff == "":
                        buff = buff[:-1]
                    elif char in ['s', 'S'] and return_state == S5_TEST:
                        # Whenever S is pressed while in testing, need to quit immediately
                        s5_end_sig = True
                        state = S5_TEST
                    elif char in ['.'] and ('.' not in buff):
                        buff += '.'

                    # Check if it's enter, convert to float and assign
                    elif char in ['\r', '\n']:
                        if (buff == ""):                    # to target .share, then return to return_state
                            # This is a form of protection so that if they press enter on an empty string it won't accept it
                            pass
                        else:
                            val = float(buff)
                            print(f"Stored: {val}")
                            inputTar.write(val)
                            state = return_state
                            return_state = None
                            # inputTar.write(False)
                            new_duty = True
                            # Clear the buffer so its ready for next time
                            buff = ""

                    # Print the buffer after each character input
                    print(buff)

            elif state == S6_SRESP:
                # First prompt user for proportional gain
                # Then, prompt user for derivative gain
                # Run step response for 3 sec window or until user interrupts with s key

                # Read in any chars from the keyboard
                if serial_port.any():
                    read_char = serial_port.read(1).decode()

                    if read_char in ['s', 'S']:
                        # End data collection prematurely
                        end_step_premature = True

                if not setGain:
                    # Disable control, motors, and set target positions to 0, 0
                    cont_enable.write(False)
                    roll_target_duty.write(0)
                    pitch_target_duty.write(0)
                    roll_target_pos.write(0)
                    pitch_target_pos.write(0)

                    state = S4_INPUT
                    print("Please enter a proportional gain Kp for the controller:")
                    inputTar = prop_gain
                    return_state = S6_SRESP
                    setGain = True
                    buff = ""

                elif not setDGain:
                    state = S4_INPUT
                    print("Please enter a derivative gain Kd for the controller:")
                    inputTar = deriv_gain
                    return_state = S6_SRESP
                    setDGain = True
                    buff = ""
                    # don't think this is needed as no driver is in use
                    # # need to enable controller
                    # cFlag.write(True)

                elif setGain and setDGain:
                    # We are now starting the step response
                    # print("Starting Step Response")

                    # When step_start_time is None we are just starting the response
                    if step_start_time is None:
                        print("Starting Step Response")
                        step_start_time = current_time
                        num_data_points = math.floor(
                            (3 * 10 ** 6) / step_sample_period)
                        test_roll_data = array.array('d', num_data_points*[0])
                        test_pitch_data = array.array('d', num_data_points*[0])
                        sample_number = 0
                        step_sample_next = step_start_time + step_sample_period

                    else:
                        try:
                            if ticks_diff(current_time, step_sample_next) >= 0:
                                # time to collect a new data point

                                test_roll_data[sample_number] = euler_angles['EUL_ROLL']
                                test_pitch_data[sample_number] = euler_angles['EUL_PITCH']
                                sample_number += 1
                                step_sample_next += step_sample_period

                        except MemoryError:
                            raise MemoryError(
                                "Error in filling up test data array")

                        # When less than 1 second, set duty cycle to 0
                        if (ticks_diff(current_time, step_start_time) < (10 ** 6)) and not end_step_premature:
                            # Should be disabled
                            cont_enable.write(False)
                        elif (ticks_diff(current_time, step_start_time) < (3 * 10 ** 6)) and not end_step_premature:
                            # Enable the controller
                            cont_enable.write(True)
                        else:
                            # We have exceeded the 3 seconds. Lets stop the motor
                            cont_enable.write(False)
                            roll_target_duty.write(0)
                            pitch_target_duty.write(0)
                            print("Step Response Finished")

                            # Write our data to the print queue
                            new_job = PrintJob("X Position, Y Position")
                            print_queue.add_job(new_job)

                            for i in range(len(test_roll_data)):
                                print_queue.add_job(
                                    PrintJob(f"{test_roll_data} {test_pitch_data}"))

                            # We're done collecting so lets finish up the State and return to S1_CMD

                            state = S1_CMD

                            setGain = False
                            setDGain = False

                            step_start_time = None
                            end_step_premature = False

                            sample_number = 0
                            test_roll_data = None
                            test_pitch_data = None

            else:
                raise ValueError(
                    f"Invalid state value in {task_name}: State {state} does not exist")

            yield state

        else:
            # Ticks diffnot yet ready to change state
            yield None
