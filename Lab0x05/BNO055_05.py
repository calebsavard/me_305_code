'''!@file       BNO055_05.py
    @brief      Driver class for BNO055 IMU
    @details	For representing a BNO055 inertial measurement unit.
    @author	    Chris Linthacum
    @author     Caleb Savard
    @date       February 24, 2022
'''

import pyb
from time import sleep
import micropython
import struct

IMU_MODE = micropython.const(0b1000)
COMPASS_MODE = micropython.const(0b1001)
M4G_MODE = micropython.const(0b1010)
MDOF_FMC_OFF_MODE = micropython.const(0b1011)
NDOF_MODE = micropython.const(0b1100)


class BNO055_05:
    '''!@brief      A class to represent a BNO055 IMU.
        @details    Objects of this class can modify fusion modes of IMU, retrieve calibration status 
                    from the IMU, write/retrieve calibration coefficients, read Euler angles, and 
                    read angular velocity from the IMU.
    '''

    def __init__(self, i2c: pyb.I2C, device_addr):
        '''!@brief      Creates a new BNO055 object
            @details    Initialized the BNO055 object with the connected pins.

            @param  i2c     a pyb.I2C object to connect to the BNO055 device
            @param  device_addr     The device address (often in hex)

        '''
        
        # Takes in the pyb.I2C object preconfigured in controller mode
        self._i2c = i2c
        self._i2c_addr = device_addr
        

    def calibration_status(self):
        '''!@brief      Returns the calibration status bytes from the IMU
            @details    
            @return     Dictionary of calibration status values
        '''

        status_byte = self._i2c.mem_read(1, self._i2c_addr, 35)
        statuses = {}

        statuses['SYS_calib_status'] = (status_byte[0] & 0b11000000) >> 6
        statuses['GYR_calib_status'] = (status_byte[0] & 0b00110000) >> 4
        statuses['ACC_calib_status'] = (status_byte[0] & 0b00001100) >> 2
        statuses['MAG_calib_status'] = status_byte[0] & 3

        # print(f"SYS: {statuses['SYS_calib_status']}, GYRO: {statuses['GYR_calib_status']}, ACC: {statuses['ACC_calib_status']}, MAG: {statuses['MAG_calib_status']}")
        return statuses

    def change_op_mode(self, op_mode):
        '''!@brief      Changes the operation mode to one of many fusion modes
            @details    By passing an operation mode constant (IMU_MODE, COMPASS_MODE, M4G_MODE, MDOF_FMC_OFF_MODE, MDOF_MODE) change the operating
                        mode of the IMU by writing to memory.
            @param op_mode      The operating mode constant to set the device in. (IMU_MODE, COMPASS_MODE, M4G_MODE, MDOF_FMC_OFF_MODE, MDOF_MODE)

        '''
        self._i2c.mem_write(op_mode, self._i2c_addr, 0x3d)


    def read_calib_coeff(self):
        '''!@brief      Returns the calibration coefficients of the IMU
            @details    Returns the calibration coefficients of the IMU as an array of packed binary data

            @return     Returns calibration coefficients as packed binary data
        '''
        # gyr_calib_coeffs = self._i2c.mem_read()

        data = self._i2c.mem_read(22, self._i2c_addr, 0x55)

        return data

    def write_calib_coeff(self, data):
        '''!@brief      Writes the packed binary calibration coefficients to the IMU.
            @details    Takes calibration coefficients passed as data as an argument and writes to the IMU

            @param  data    Packed binary data containing the calibration coefficients for the IMU

        '''

        data = self._i2c.mem_write(data, self._i2c_addr, 0x55)


    def read_euler_angles(self):
        '''!@brief      Returns the Euler angles from the IMU
            @details    Returns the Euler angles from the IMU as dictionary

            @return data        A dictionary of Euler angle values converted into degrees.
        '''


        raw_data = self._i2c.mem_read(6, self._i2c_addr, 0x1A)
        data = {}
        (data['EUL_HEADING'],
         data['EUL_ROLL'],
         data['EUL_PITCH']) = struct.unpack("hhh", raw_data)
        
        data['EUL_ROLL'] = -1*data['EUL_ROLL']
        data['EUL_PITCH'] = -1*data['EUL_PITCH']

        # convert data to degrees
        for key in data:
            data[key] = data[key] / 16

        return data

    def read_angular_velocity(self):
        '''!@brief      Returns the angular velocities from IMU.
            @details    Returns the angular velocities from the IMU in degrees per second.

            @return data        A dictionary of angular velocities. No units specified.
        '''

        raw_data = self._i2c.mem_read(6, self._i2c_addr, 0x14)
        data = {}
        (data['GYR_DATA_Y'],
         data['GYR_DATA_X'],
         data['GYR_DATA_Z']) = struct.unpack("hhh", raw_data)
        
        data['GYR_DATA_X'] = -1*data['GYR_DATA_X']
        data['GYR_DATA_Y'] = -1*data['GYR_DATA_Y']

        # convert data to degrees
        for key in data:
            data[key] = data[key] / 16

        return data


if __name__ == '__main__':
    i2c = pyb.I2C(1, pyb.I2C.CONTROLLER)
    print(f"{i2c.mem_read(1, 0x28, 0)}")

    imu = BNO055_05(i2c, 0x28)

    imu.change_op_mode(NDOF_MODE)

    i = 0
    while True:
        print(f"i: {i}")
        data = imu.read_euler_angles()
        print(f"EUL_HEAD: {data['EUL_HEADING']}, EUL_ROLL: {data['EUL_ROLL']}, EUL_PITCH: {data['EUL_PITCH']}")
        sleep(1)
        i += 1
