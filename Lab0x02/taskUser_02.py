'''!@file           taskUser_02.py
    @brief          Manages the states for the User Interface task.
    @details        Manages the current state and executes state-related logic for 
                    all User Interface functionality. Cycles through states for processing
                    input, zeroing encoder position, and printing output.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           February 2, 2022
'''

from time import ticks_us, ticks_add, ticks_diff
from pyb import USB_VCP
import micropython
import shares_02

S0_INIT = micropython.const(0)
S1_CMD = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_PRINT = micropython.const(3) 

def printLegend_02():
    '''!
    @brief          Prints the available user key commands to the console
    @details        Prints a table of available user input commands to the console to
                    preview the available user key commands.
    '''
    print("-----------------------------------------")
    print("|        Available User Commands        |")
    print("|---------------------------------------|")
    print("| z | Zero the position of encoder 1    |")
    print("|---------------------------------------|")
    print("| p | Print out position of encoder 1   |")
    print("|---------------------------------------|")
    print("| d | Print out the delta for encoder 1 |")
    print("|---------------------------------------|")
    print("| g | Collect encoder 1 data for 30     |")
    print("|   | seconds and print to PuTTY as     |")
    print("|   | comma separated list              |")
    print("|---------------------------------------|")
    print("| s | End data collection prematurely   |")
    print("-----------------------------------------")
    print("\n")



def taskUserFcn_02(task_name: str, period: int, zFlag: shares_02.Share_02, pFlag: shares_02.Share_02, dFlag: shares_02.Share_02, gFlag: shares_02.Share_02, sFlag: shares_02.Share_02, dataPrint: shares_02.Share_02):
    '''!@brief              Main task function to control UI states.
        @details            Manage different User Input states, including Init, Read_Cmd, Zero_Encoder, and Print_Data.
                            On function run, executes the logic of the current state and if appropriate shifts 
                            state for next run of function.
        @param task_name    Task name for the function to help with debugging
        @param period       Period to run execute function at. Period defines frequency that states are executed and refreshed.
        @param zFlag        Shared data object to encapsulate the z key being pressed. Signals that encoder should be zeroed. 
        @param pFlag        Shared data object to encapsulate the p key being pressed. Signals to print the encoder position.
        @param dFlag        Shared data object to encapsulate the d key being pressed. Signals to print the encoder delta.
        @param gFlag        Shared data object to encapsulate the g key being pressed. Signals to begin data collection.
        @param sFlag        Shared data object to encapsulate the s key being pressed. Signals to end data collection prematurely.
        @param dataPrint    Shared data object to store the recorded encoder data. Stores both the position data and time values.
        @yield state        If task ran, yields state the function is now in. If task did not evaluate, returns None.
    '''

    state = 0
    index_to_print = 0

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    serial_port = USB_VCP()

    while True:

        current_time = ticks_us()

        if (ticks_diff(current_time, next_time) >= 0):

            next_time = ticks_add(next_time, period)

            if state == S0_INIT:

                ## Print out the introduction information and the legend here
                print("Welcome to the program.\n")
                printLegend_02()
                state = S1_CMD

            elif state == S1_CMD:

                # print(f"State = {state}")
                ## Check to see if we have anything to print.
                ## If we have long data, switch the state
                if dataPrint.read() is not None:
                    state = S3_PRINT

                # print("Checking Position")
                if pFlag.readData() is not None:
                    print("Print Pos:")
                    print(f"Encoder Position = {pFlag.readData()}")
                    pFlag.storeData(None)

                if dFlag.readData() is not None:
                    print(f"Encoder Delta = {dFlag.readData()}")
                    dFlag.storeData(None)

                
                ## Here we continously check for user input. We also process the user
                ## input checking for values and setting the appropriate flags

                if serial_port.any():
                    read_char = serial_port.read(1).decode()

                    if read_char in ['z', 'Z']:
                        # Set flags to zero position of encoder
                        zFlag.write(True)
                        state = S2_ZERO

                    elif read_char in ['p', 'P']:
                        # Set flag to print out position of encoder
                        pFlag.write(True)

                    elif read_char in ['d', 'D']:
                        # Set flag to print out delta of encoder
                        dFlag.write(True)

                    elif read_char in ['g', 'G']:
                        # Collect encoder data 
                        gFlag.write(True)

                    elif read_char in ['s', 'S']:
                        # End data collection prematurely
                        sFlag.write(True)

                    else:
                        print(f"Invalid character '{read_char}' entered\n")
                        # Clear the input buffer
                        serial_port.read()
                        printLegend_02()

            elif state == S2_ZERO:
                # Wait for zFlag to be set to false to return to state 1
                if not zFlag.read():
                    print("Done.")
                    state = S1_CMD

            elif state == S3_PRINT:
                # Iterate through printing until reached the end 
                time_data = dataPrint.readData()
                data = dataPrint.read()


                if index_to_print < len(data):
                    print(f"({time_data[index_to_print]}, {data[index_to_print]})")
                    index_to_print += 1
                else:
                    index_to_print = 0
                    dataPrint.write(None)
                    state = S1_CMD
            
            else:
                raise ValueError(f"Invalid state value in {task_name}: State {state} does not exist")

            yield state

        else:
            # Ticks diffnot yet ready to change state
            yield None
