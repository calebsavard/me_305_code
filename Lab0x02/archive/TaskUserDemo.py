'''!@file       TaskUserDemo.py
    @brief      A demo of how to implement a User Interface task as an FSM.
    @details    The task uses the USB VCP (Virtual COM Port) to cooperatively 
                take character input from the user working at a serial terminal
                such as PuTTY.
    @author     Charlie Refvem  
    @date       01/26/2022
    @copyright  Copyright C. Refvem 2022. Use of this file is permitted in ME305
                as an example only.
'''

# For this task we will need to be able to control task timing, so we need
# access to functions from the time module. We will also need to access the USB
# Virtual COM Port for character IO.
from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP

def taskFunction(taskName, period):
    '''!@brief              A generator to implement the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
    '''
    
    # The state of the FSM to run on the next iteration of the generator.
    state = 0
        
    # On the entry to state 0 we can print a message for the user describing the
    # UI.
    print("ME 305 Demo User Interface")
    print("Press a key to begin. Pressing 1 will transition to state 1 of the FSM")
    
    # A timestamp, in microseconds, indicating when the next iteration of the
    # generator must run.
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    # A (virtual) serial port object used for getting characters cooperatively.
    serport = USB_VCP()
    
    # The finite state machine must run indefinitely.
    while True:
        # We should only call the ticks_us() function once per iteration of the 
        # of the task to "timestamp" the task iteration. This value can be used 
        # internally within the states if timestamps are require for data
        # collection.
        current_time = ticks_us()
    
        # The finite state machine only needs to run if the interval has elapsed
        # as indicated by current_time being greater than next_time. We can'tell
        # do direct comparison or arithmetic of ticks_us() values because of the
        # internal rollover/overflow of the ticks value. Therefore we have to
        # use ticks_diff() to find the difference in ticks value which then can 
        # be compared to zero to check which is greater.
        if ticks_diff(current_time,next_time) >= 0:
        
            # Once the task does need to run we must prepare for the next
            # iteration by adjusting the value of next_time by adding the task
            # interval. Again we can't use normal arithmetic so the ticks_add()
            # function is used.
            next_time = ticks_add(next_time, period)
            
            # State 0 code
            if state == 0:
                # If a character is waiting, read it
                if serport.any():
                    # Read one character and decode it into a string. This
                    # decoding converts the bytes object to a standard Python
                    # string object.
                    charIn = serport.read(1).decode()
                    
                    # The value of the received character is then compared to
                    # the command we are looking for in order to determine state
                    # transitions
                    if charIn == '1':
                        print("Transitioning to state 1")
                        state = 1 # transition to state 1
                    else:
                        print(f"You typed {charIn} from state 0 at t={ticks_diff(current_time,start_time)/1e6}[s].")
            
            # State 1 code
            elif state == 1:
                # If a character is waiting, read it
                if serport.any():
                    # Read one character and decode it into a string
                    charIn = serport.read(1).decode()
                    
                    if charIn == '0':
                        print("Transitioning to state 0")
                        state = 0 # transition to state 0
                    elif charIn == '2':
                        print("Transitioning to state 2")
                        state = 2 # transition to state 2
                    else:
                        print(f"You typed {charIn} from state 1 at t={ticks_diff(current_time,start_time)/1e6}[s].")
                    
            # Ideally we should never reach this state unless an error has
            # occurred. In the example it is possible to trigger an error by
            # typing the character '2' while in state 1 of the task.
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            
            # After a valid run of the state machine we can yield the state.
            # This yielded value could be stored in the main loop below to trace
            # the state transitions.
            yield state
        
        # If the time has not come yet to run the task we can just exit early by
        # yielding nothing.
        else:
            yield None

# The following code would usually belong in main.py but is utilized here to
# test the task in isolation from other tasks. Eventually this will not be
# practical once the tasks interface with eachother using shared variables.
if __name__ == '__main__':
    
    # A generator object created from the generator function for the user
    # interface task.
    task1 = taskFunction('T1', 10_000)
    
    # To facilitate running of multiple tasks it makes sense to create a task
    # list that can be iterated through. Adding more task objects to the list
    # will allow the tasks to run together cooperatively.
    taskList = [task1]
    
    # The system should run indefinitely until the user interrupts program flow
    # by using a Ctrl-C to trigger a keyboard interrupt. The tasks are run by
    # using the next() function on each item in the task list.
    while True:
        try:
            for task in taskList:
                next(task)
        
        except KeyboardInterrupt:
            break
            
    # In addition to exiting the program and informing the user any cleanup code
    # should go here. An example might be un-configuring certain hardware
    # peripherals like the User LED or perhaps any callback functions.
    print('Program Terminating')