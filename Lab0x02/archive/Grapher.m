clear
close all

str = fileread('putty.log');
str = strrep(strrep(str, '(',''), ')','');
str = strrep(str, ' ','');
str = strrep(str, ',',' ');
P = sscanf(str,'%d',[2 Inf])';

figure
plot(P(:,1), P(:,2))
title('Encoder position over time')
xlabel('Time [\mus]')
ylabel('Position [encoder ticks]')
