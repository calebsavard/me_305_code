from taskEncoder import taskEncoderFcn
import shares
from time import ticks_us, ticks_add, ticks_diff


# This function is probably not needed without data collection being a separate state

def taskFlag(taskName, period, gFlag):

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    while True:
        current_time = ticks_us()

        if ticks_diff(current_time, next_time) >= 0:
            next_time = ticks_add(next_time, period)

            gFlag.write(True)

        yield gFlag


if __name__ == '__main__':

    zFlag = shares.Share(False)
    pFlag = shares.Share(False)
    dFlag = shares.Share(False)
    gFlag = shares.Share(False)
    sFlag = shares.Share(False)
    task1 = taskEncoderFcn('T1', 100_000, zFlag, pFlag,
                           dFlag, gFlag, sFlag, 30_000_000)
    task2 = taskFlag('T2', 5000_000, zFlag)

    while True:

        try:
            next(task1)
            next(task2)

        except KeyboardInterrupt:
            break

    print("Program terminated by user... goodbye. :(")
