from time import ticks_us, ticks_add, ticks_diff
from pyb import USB_VCP
import pyb
import micropython
import shares
import encoder

S0_INIT = micropython.const(0)
S1_UPDATE = micropython.const(1)
S2_ZERO = micropython.const(2)


def taskEncoderFcn(task_name: str, period: int, zFlag: shares.Share, pFlag: shares.Share, dFlag: shares.Share, gFlag: shares.Share):
    '''!@file
    
    '''

    state = 0

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    encoder1 = encoder.Encoder(4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)


    while True:

        current_time = ticks_us()

        if (ticks_diff(current_time, next_time) >= 0):

            next_time = ticks_add(next_time, period)

            if state == S0_INIT:
                ## Want to do init stuff here
                state = S1_UPDATE

            elif state == S1_UPDATE:
                ## Update the current position
                encoder1.update()

                if zFlag.read():
                    state = S2_ZERO

                if pFlag.read():
                    print(f"Encoder Postion = {encoder1.get_position()}")

                if dFlag.read():
                    print(f"Encoder Delta = {encoder1.get_delta()}")
                
                if gFlag.read():
                    # Collect data from the encoder when True
                    pass

                elif ((gFlag.read() == False) and (collected_data not None)):
                    
                    printData(collected_data)
                    collected_data = None
                


            elif state == S2_ZERO:
                ## Zero the encoder
                encoder1.zero()
                zFlag.write(False)
                state = S1_UPDATE
            





        else:
            yield None
