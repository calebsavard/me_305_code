"""
    @file fibonacci.py
    Author: Chris Linthacum
    Class: ME305-02
"""

def user_prompt() -> int:
    '''
    @brief      Requests a user input for Fibonacci index
    @param      None
    '''

    user_input = input("Please enter the index for the desired Fibonacci number: \n")
    # The isdigit() function ensures both that the input is an integer and that the input was a positive number.
    if (not user_input.isdigit()):
        print("That is an invalid index selection. Index should be an integer greater than or equal to 0.\n")
        user_input = user_prompt()
    
    return int(user_input)

def fib(target):
    '''
    @brief      Computes a Fibonacci number using a list.
    @params     target      The index of the desired Fibonacci number.
    '''
    # Set the first two values of the Fib Sequence
    fib_seq = [0, 1]

    # Return the value if index = 0, otherwise iterate over a range to calculate the Fib sequence to length = target.
    if target == 0:
        return fib_seq[target]
    for i in range(target - 1):
        fib_seq.append(fib_seq[-1] + fib_seq[-2])

    # Return the last value of the Fib sequence list
    return fib_seq[-1]


def test_fibonacci():
    '''
    @brief      Tests the fib() function
    @param      None
    '''

    # Asserts the first several values of the Fib sequence to test whether the function works.
    assert(fib(0) == 0)
    assert(fib(1) == 1)
    assert(fib(2) == 1)
    assert(fib(3) == 2)
    assert(fib(4) == 3)
    assert(fib(5) == 5)
    assert(fib(6) == 8)
    assert(fib(7) == 13)
    assert(fib(8) == 21)
    assert(fib(9) == 34)
    assert(fib(10) == 55)
    assert(fib(11) == 89)
    assert(fib(12) == 144)
    assert(fib(13) == 233)
    assert(fib(14) == 377)

def main():
    '''
    @brief      The main function that will loop prompting the user for input and calculating the Fibonacci number.
    @param      None
    '''
    while True:
        # Prompt the user for an input
        user_input = user_prompt()

        # Print the result out
        print("The Fibonacci number at index {index} is {value}.\n".format(index=user_input, value=fib(user_input)))
        
        # Allow the user to exit, otherwise loop to repeat.
        exit_choice = input("Press any key to continue or x to exit. \n")
        if exit_choice.lower() == 'x':
            break

if __name__ == '__main__':
    main()