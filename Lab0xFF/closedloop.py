'''!@file           closedloop.py
    @brief          Class for a PID controller.
    @details        Class for a PID containing update() and set_K() methods for Kp, Ki, and Kd.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           March 16, 2022
'''
import shares

class ClosedLoop:
    '''!@brief      Driver class for a Closed Loop Controller
        @details    Used to control a signal and provide output to achieve desired steady state
    
    '''

    def __init__(self, 
                position_signal: shares.Share, 
                deriv_signal: shares.Share,
                target_pos: shares.Share, 
                target_deriv: shares.Share,
                prop_gain: float,
                deriv_gain: float,
                int_gain: float,
                sat_limit_low: int, 
                sat_limit_high: int,
                integral_limit: int = None,
                integral_influence_limit: int = None,
                controller_signals: shares.Share = None,
                integral_limit_region: int = None):
        '''!@brief                  Initializes the ClosedLoop object.     
            @details                Objects of this class act as a PID Controller, 
                                    taking in an input signal and target value, along with a proportional,
                                    integral, and differential gain. 
            @param  position_signal      Share object containing the position input signal to the controller.
            @param  deriv_signal    Share object containing the input derivative signal for the controller.
            @param  target_pos      Share object containing the desired reference value for the controller position.
            @param  target_deriv    Share object containing the desired reference value for controller velocity.
            @param  prop_gain       Float value for the Kp for the controller.
            @param  deriv_gain      Float value for the Kd for the controller.
            @param  int_gain        Float value fot the Ki for the controller.
            @param  sat_limit_low   Minimum value to saturate the controller output at.
            @param  sat_limit_high  Maximum value to saturate the controller output at.
            @param  integral_limit  Optional parameter to specify limit to how much build-up can occur in the integral term
            @param  integral_influence_limit    Optional parameter to specify the maximum value the integral term (Ki * integral) can contribute to the output signal
            @param  controller_signals          Shared object to encapsulate the controller term values (prop, deriv, integral) to pass to user interface for easier debugging
            @param  integral_limit_region       Integer limiting the region where integral term has any effect. An input signal with absolute value greater than this value will have no integral term in the output signal.
            
        '''

        self._position_signal = position_signal
        self._deriv_signal = deriv_signal
        self._target_pos = target_pos
        self._target_deriv = target_deriv
        self._Kp = prop_gain

        self._Kd = deriv_gain

        self._Ki = int_gain
        self._integral = 0

        self._sat_low = sat_limit_low
        self._sat_high = sat_limit_high

        self._integral_limit = integral_limit
        self._integral_influence_limit = integral_influence_limit

        self._controller_signals = controller_signals

        self._integral_limit_region = integral_limit_region

    def update(self):
        '''!@brief          Returns an output signal based on the initialized share variables
            @details        Multiplies the error signal by the proportional gain and adds the velocity/derivitive error signal to obtain an output signal.
                            If the output signal exceeds the saturation limits, return the saturation limit instead.
        '''
        
        error = (self._target_pos.read() - self._position_signal.read())
        deriv = (self._target_deriv.read() - self._deriv_signal.read())
        self._integral = self._integral + error

        if (self._integral_limit is not None):
            if (self._integral > self._integral_limit):
                self._integral = self._integral_limit
            elif (self._integral < -self._integral_limit):
                self._integral = -self._integral_limit
        # print(self._integral * self._Ki / 1000)

        prop_val = self._Kp * error
        deriv_val = self._Kd * deriv
        int_val = self._Ki * (self._integral)

        if self._integral_influence_limit is not None:
            if (int_val > self._integral_influence_limit):
                int_val = self._integral_influence_limit
            if (int_val < -(self._integral_influence_limit)):
                int_val = -(self._integral_influence_limit)

        if self._integral_limit_region is not None:
            if (self._position_signal.read() > self._integral_limit_region):
                int_val = 0
            if (self._position_signal.read() < -(self._integral_limit_region)):
                int_val = 0

        if self._controller_signals is not None:
            self._controller_signals.write({'prop_val': prop_val, 'deriv_val': deriv_val, 'int_val': int_val})

        
        
        out_signal = prop_val + deriv_val + int_val
        # out_signal = self._Kp * (self._target_pos.read() - self._position_signal.read())
        # print(
            # f"Out Signal: {out_signal}\nPosition Error: {(self._target_pos.read() - self._position_signal.read())}\n Derivative Error: {(self._target_deriv.read() - self._deriv_signal.read())}")
        if out_signal > self._sat_high:
            out_signal = self._sat_high
        elif out_signal < self._sat_low:
            out_signal = self._sat_low

        # print(f"Position Signal: {self._position_signal.read()}")

        # print(f"Out signal: {out_signal}")
        
        return out_signal


    def set_Kp(self, new_Kp):
        '''!@brief          Sets the Kp to a new value
            @details        Replaces the existing Kp with a new value.
            @param  new_Kp  The new value of Kp (proportional gain) to be set.
            
        '''
        self._Kp = new_Kp   

    def set_Kd(self, new_Kd):
        '''!@brief          Sets the Kd to a new value
            @details        Replaces the existing Kd with a new value.
            @param  new_Kd  The new value of Kd (derivative gain) to be set.
            
        '''

        self._Kd = new_Kd

    def set_Ki(self, new_Ki):
        '''!@brief          Sets the Ki to a new value
            @details        Replaces the existing Ki with a new value.
            @param  new_Ki  The new value of Ki (integral gain) to be set.
            
        '''

        self._Ki = new_Ki

