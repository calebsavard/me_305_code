'''!@file           taskIMU.py
    @brief          Creates and operates the IMU.
    @details        Instantiates an I2C device for and IMU (BNO055 in particular). 
                    The pyBoard is set as the controller by default and the IMU is
                    set to NDOF mode by default. A state diagram of the task can be seen below.<br>
                    
                    <img src="Resources/FFtaskIMUState" alt="taskIMU State Diagram" width="600"><br/> 
                    
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           March 2, 2022
'''

# Import stuff
import BNO055
from printqueue import PrintQueue, PrintJob
import pyb
from time import ticks_us, ticks_add, ticks_diff
import micropython
import os
import shares

# State names
##  @brief      State 0
#   @details    Descriptive name for State 0, the initializing state.
S0_INIT = micropython.const(0)
##  @brief      State 1
#   @details    Descriptive name for State 1, the calibration state.
S1_CALIB = micropython.const(1)
##  @brief      State 2
#   @details    Descriptive name for State 2, the run state.
S2_RUN = micropython.const(2)


def taskIMUFcn(taskName: str,
               period: int,
               euler_angles: shares.Share,
               angular_velocities: shares.Share,
               imu_calib_status: shares.Share,
               print_queue: PrintQueue):
    '''!@brief              Task for operating the IMU
        @details            Finite state machine that controls IMU creation, calibration, and operation.
        @param  taskName    Brief string to describe the instance of the function. Useful for debug purposes.
        @param  period      Period with which to run the function and update/execute state logic.
        @param  euler_angles    Shared variable to pass euler angles to other tasks
        @param  angular_velocities  Shared variable to pass angular velocities to other tasks
        @param  imu_calib_status    Shared variable to pass IMU calibration status to other tasks
        @param  print_queue         A print queue object to pass print jobs to the taskUser function to be printed
    '''

    # Initialize, create object
    # print("Creating IMU object")
    IMU1 = BNO055.BNO055(pyb.I2C(1, pyb.I2C.CONTROLLER), 0x28)
    NDOF_MODE = micropython.const(0b1100)
    # print('Entering NDOF mode')
    IMU1.change_op_mode(NDOF_MODE)

    state = 0
    statuses = None

    # Start timestamp system
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    while True:
        current_time = ticks_us()

        if ticks_diff(current_time, next_time) >= 0:

            next_time = ticks_add(next_time, period)

            if state == S0_INIT:

                state = S1_CALIB

                # Need to read angles to avoid NoneType error
                euler_angles.write(IMU1.read_euler_angles())
                angular_velocities.write(IMU1.read_angular_velocity())

                # Set calibration status to false by defauly
                imu_calib_status.write(False)

            elif state == S1_CALIB:

                # If calibration file exists, read it and assign it.
                
                if "IMU_cal_coeffs.txt" in os.listdir():

                    print_queue.add_job(PrintJob('IMU Calibration file found'))
                    with open("IMU_cal_coeffs.txt", 'r') as f:

                        cal_data_string = f.readline()

                        cal_values = [
                            cal_value for cal_value in cal_data_string.strip().split(',')]

                        # print(IMU1.read_calib_coeff())

                        # raise ValueError("Intentional Stop")

                        # [Call the IMU object to write these values to the right directories]

                    imu_calib_status.write(True)
                    print_queue.add_job(PrintJob('Loaded calibration values.'))

                    # Since we are calibrated change state
                    state = S2_RUN

                # If there's no calibration file manually calibrate
                else:
                    # If it's the first time, print calibration message and read coefficients.
                    if not statuses:

                        print_queue.add_job(
                            PrintJob('Calibration file not found, calibrating...'))
                        statuses = IMU1.calibration_status()

                    # If it's not the first time, update statuses unless calibration is complete.
                    elif (statuses['SYS_calib_status'] != 3) or \
                            (statuses['GYR_calib_status'] != 3) or \
                            (statuses['ACC_calib_status'] != 3) or \
                            (statuses['MAG_calib_status'] != 3):

                        statuses = IMU1.calibration_status()
                        print_queue.add_job(PrintJob(
                            f"SYS: {statuses['SYS_calib_status']}, GYRO: {statuses['GYR_calib_status']}, ACC: {statuses['ACC_calib_status']}, MAG: {statuses['MAG_calib_status']}", 20_000))

                    # Otherwise (if it's done), write calibration constants and clear statuses
                    else:
                        print_queue.add_job(PrintJob('Done Calibrating'))
                        print_queue.add_job(PrintJob(
                            f"SYS: {statuses['SYS_calib_status']}, GYRO: {statuses['GYR_calib_status']}, ACC: {statuses['ACC_calib_status']}, MAG: {statuses['MAG_calib_status']}"))

                        with open("IMU_cal_coeffs.txt", 'w') as f:

                            (ACC_OFFSET_X_LSB, ACC_OFFSET_X_MSB, ACC_OFFSET_Y_LSB, ACC_OFFSET_Y_MSB, ACC_OFFSET_Z_LSB, ACC_OFFSET_Z_MSB,
                             MAG_OFFSET_X_LSB, MAG_OFFSET_X_MSB, MAG_OFFSET_Y_LSB, MAG_OFFSET_Y_MSB, MAG_OFFSET_Z_LSB, MAG_OFFSET_Z_MSB,
                             GYR_OFFSET_X_LSB, GYR_OFFSET_X_MSB, GYR_OFFSET_Y_LSB, GYR_OFFSET_Y_MSB, GYR_OFFSET_Z_LSB, GYR_OFFSET_Z_MSB,
                             ACC_RADIUS_LSB, ACC_RADIUS_MSB, MAG_RADIUS_LSB, MAG_RADIUS_MSB) = IMU1.read_calib_coeff()

                            f.write(f"{ACC_OFFSET_X_LSB}, {ACC_OFFSET_X_MSB}, {ACC_OFFSET_Y_LSB}, {ACC_OFFSET_Y_MSB}, {ACC_OFFSET_Z_LSB}, {ACC_OFFSET_Z_MSB}, \
                                    {MAG_OFFSET_X_LSB}, {MAG_OFFSET_X_MSB}, {MAG_OFFSET_Y_LSB}, {MAG_OFFSET_Y_MSB}, {MAG_OFFSET_Z_LSB}, {MAG_OFFSET_Z_MSB}, \
                                    {GYR_OFFSET_X_LSB}, {GYR_OFFSET_X_MSB}, {GYR_OFFSET_Y_LSB}, {GYR_OFFSET_Y_MSB}, {GYR_OFFSET_Z_LSB}, {GYR_OFFSET_Z_MSB}, \
                                    {ACC_RADIUS_LSB}, {ACC_RADIUS_MSB}, {MAG_RADIUS_LSB}, {MAG_RADIUS_MSB}\r\n")

                        statuses = None

                        print_queue.add_job(PrintJob('Calibration Complete.'))

                        imu_calib_status.write(True)

                        # raise ValueError("Calibs written")

                        state = S2_RUN

            elif state == S2_RUN:

                # Always write the updated euler angles to the shared variable
                euler_angles.write(IMU1.read_euler_angles())

                # print(euler_angles.read())

                # if pFlag.read() == True:
                #     pFlag.storeData(IMU1.read_angles())

                #     pFlag.write(False)

                # Always write the updated angular velocities to shared variable
                angular_velocities.write(IMU1.read_angular_velocity())

                # print(angular_velocities.read())

                # # If vFlag is true, read the angular velocities and write them to vFlag
                # if vFlag.read() == True:
                #     vFlag.storeData(IMU1.read_velocies())

                #     vFlag.write(False)

            # Error if there's a problem with the state machine
            else:

                raise ValueError('Invalid State!')

            yield state


def testPrint(period: int, euler_angles: shares.Share, angular_velocities: shares.Share, imu_calib_status: shares.Share):
    '''!@brief      Simple printing task for the testing suite
        @details    Simple printing task for the testing suite. Emulates functionality found in taskUser
        @param      period          Rate to run the print function
        @param      euler_angles    Share variable to pass euler angles to emulate main program
        @param      angular_velocities  Shared variable to pass angular velocities to emulate main program
        @param      imu_calib_status    Shared variable to pass imu calibration status to emulate main program
    '''

    # Start timestamp system
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    while True:
        current_time = ticks_us()

        if ticks_diff(current_time, next_time) >= 0:

            next_time = ticks_add(next_time, period)

            if imu_calib_status.read():

                angles = euler_angles.read()
                print("Euler angles:")
                print(
                    f"   Heading: {angles['EUL_HEADING']}, Roll: {angles['EUL_ROLL']}, Pitch: {angles['EUL_PITCH']}")
                velocities = angular_velocities.read()
                print("Angular Velocities:")
                print(
                    f"   Gyro X: {velocities['GYR_DATA_X']}, Gyro Y: {velocities['GYR_DATA_Y']}, Gyro Z: {velocities['GYR_DATA_Z']}")

        yield


if __name__ == '__main__':

    ## Euler angles share for the testing suite
    euler_angles = shares.Share(None)
    ## Angular velocities share for the testing suite
    angular_velocities = shares.Share(None)
    ## Calibration status variable for the testing suite
    imu_calib_status = shares.Share(False)
    ## Print queue for testing suite
    print_queue = PrintQueue()

    ## Initialization of tasks for test suite
    task_list = [taskIMUFcn('TaskIMU', 10_000, euler_angles, angular_velocities, imu_calib_status, print_queue),
                 testPrint(1_000_000, euler_angles, angular_velocities, imu_calib_status)]
    while True:
        try:
            for task in task_list:
                next(task)
        except KeyboardInterrupt:
            break

    print("Terminating Program")
