'''!@file           taskAngleController.py
    @brief          Sets up and calls the closed loop controller class.
    @details        Configures a PID controller object and continuously updates the controller output value which represents the desired angle of the platform.
                    This functions as the outer loop of the control system.
    @author         Caleb Savard
    @author         Chris Linthacum
    @date           March 18, 2022
'''

import shares
from time import ticks_us, ticks_diff, ticks_add
import closedloop
from printqueue import PrintQueue

def taskAngleControllerFcn(taskName: str, 
                        period: int,
                        roll_target_pos: shares.Share,
                        pitch_target_pos: shares.Share,
                        x_position_signal: shares.Share,
                        x_pos_deriv_signal: shares.Share,
                        x_target_position: shares.Share,
                        x_pos_deriv_target: shares.Share,
                        y_position_signal: shares.Share,
                        y_pos_deriv_signal: shares.Share,
                        y_target_position: shares.Share,
                        y_pos_deriv_target: shares.Share,
                        positionx_prop_gain: shares.Share,
                        positionx_deriv_gain: shares.Share,
                        positionx_int_gain: shares.Share,
                        cont_enable: shares.Share,
                        ball_detected: shares.Share,
                        print_queue: PrintQueue,
                        positiony_prop_gain: shares.Share,
                        positiony_deriv_gain: shares.Share,
                        positiony_int_gain: shares.Share,
                        controller_signalsx: shares.Share,
                        controller_signalsy: shares.Share):
    '''!@brief      Sets up and calls the closed loop controller class.
        @details    Every period, this task resets the Kp using set_Kp and updates the controller. Saturation limits are set to 100 and -100.
        @param  taskName        Task name label for debugging purposes.
        @param  period          Period to run taskControllerFcn
        @param  roll_target_pos     Shared variable to pass out the desired platform roll angle.
        @param  pitch_target_pos    Shared variable to pass out the desired platform pitch angle.
        @param  x_position_signal   Shared variable to pass in the current X position of the ball.
        @param  x_pos_deriv_signal  Shared variable to pass in the current X velocity of the ball.
        @param  x_target_position   Shared variable to pass in the desired X position of the ball.
        @param  x_pos_deriv_target  Shared variable to pass in the desired X velocity of the ball.
        @param  y_position_signal   Shared variable to pass in the current Y position of the ball.
        @param  y_pos_deriv_signal  Shared variable to pass in the current Y velocity of the ball.
        @param  y_target_position   Shared variable to pass in the desired Y position of the ball.
        @param  y_pos_deriv_target  Shared variable to pass in the desired Y velocity of the ball.
        @param  positionx_prop_gain Shared variable to pass in Kp for the X direction.
        @param  positionx_deriv_gain    Shared variable to pass in Kd for the X direction.
        @param  positionx_int_gain  Shared variable to pass in Ki for the X direction.
        @param  cont_enable     Shared variable to store a boolean whether to enable or disable the controller.
        @param  ball_detected       Shared variable to store a boolean to indicate if the ball is on the platform.
        @param  print_queue         Print queue object for handling print statements.
        @param  positiony_prop_gain Shared variable to pass in Kp for the Y direction.
        @param  positiony_deriv_gain    Shared variable to pass in Kd for the Y direction.
        @param  positiony_int_gain  Shared variable to pass in Ki for the Y direction.
        @param  controller_signalsx Shared variable to pass in the X controller gains as a single object.
        @param  controller_signalsy Shared variable to pass in the Y controller gains as a single object.
    '''

    angle_sat_limit = 12
    integ_sat_limit = 1000

    integral_influence_limit = 3
    int_region_limit = 60

    xController = closedloop.ClosedLoop(x_position_signal, x_pos_deriv_signal, x_target_position,
                                        x_pos_deriv_target, positionx_prop_gain.read(), positionx_deriv_gain.read(), positionx_int_gain.read(), sat_limit_high=angle_sat_limit, sat_limit_low=-angle_sat_limit, integral_limit=integ_sat_limit, integral_influence_limit=integral_influence_limit, controller_signals=controller_signalsx)
    yController = closedloop.ClosedLoop(y_position_signal, y_pos_deriv_signal, y_target_position,
                                        y_pos_deriv_target, positiony_prop_gain.read(), positiony_deriv_gain.read(), positiony_int_gain.read(), sat_limit_high=angle_sat_limit, sat_limit_low=-angle_sat_limit, integral_limit=integ_sat_limit, integral_influence_limit=integral_influence_limit, controller_signals=controller_signalsy)

    next_time = ticks_add(ticks_us(), period) 
    ball_counter = 0
    
    while True:
        # Initialize the timing system
        current_time = ticks_us()
        
        if cont_enable.read():
            
            # When it's time...
            if ticks_diff(current_time, next_time) >= 0:
                # print("Running controller update")
            
                next_time = ticks_add(next_time, period)

                ##### Check to see if the ball is on the platform
                if not ball_detected.read():
                    ball_counter += 1
                    if (ball_counter > 5) and (ball_counter < 7) :
                        print_queue.quick_print("Ball not detected! Leveling platform.")
                        roll_target_pos.write(2)
                        pitch_target_pos.write(-.8)
                else:
                    if ball_counter > 5:
                        print_queue.quick_print("Found the ball!")
                    ball_counter = 0

                    ##### Roll Controller

                    # Update gains for xController
                    xController.set_Kp(positionx_prop_gain.read())
                    xController.set_Kd(positionx_deriv_gain.read())
                    xController.set_Ki(positionx_int_gain.read())

                    # Update desired angle by running controller
                    pitch_offset = - 0.8
                    pitch_target_pos.write(xController.update())

                    ##### Pitch Controller

                    # Update gains for yController
                    yController.set_Kp(positiony_prop_gain.read())
                    yController.set_Kd(positiony_deriv_gain.read())
                    yController.set_Ki(positiony_int_gain.read())

                    # Update desired angle by running controller
                    roll_offset = -1
                    roll_target_pos.write(yController.update() + roll_offset)

                
        yield None
