'''!@file       taskDataCollect.py
    @brief      For collecting data and saving it to a write file.
    @details    Handles the data collection and saving to a file.
    @author     Caleb Savard
    @author     Chris Linthacum
    @date       March 16, 2022
'''
#Import stuff
import micropython
import shares
import pyb
from motor import Motor
from time import ticks_us, ticks_add, ticks_diff
import time
import array
from printqueue import PrintQueue

##  @brief      State 0
#   @details    Maps state 0 (WAIT) to a more readable const.
S0_WAIT = micropython.const(0)
##  @brief      State 1
#   @details    Maps state 1 (COLLECT) to a more readable const.
S1_COLLECT = micropython.const(1)


def taskDataFcn(taskName: str,
                 period: int,
                 data_collect_flag: shares.Share,
                 x_position_signal: shares.Share,
                 y_position_signal: shares.Share,
                 ball_detected: shares.Share,
                 euler_angles: shares.Share,
                 print_queue: PrintQueue):
    '''!@brief                      Function to execute the state management functionality for the data collection task.
        @details                    Function manages states for the management of data collection. Collects data at sample period
                                    and writes to file after data collection is complete.
        @param  taskName            Task name string for debugging purposes.
        @param  period              Data sample period - how often the function runs.
        @param  data_collect_flag   Flag for whether data collection should be enabled or disabled.
        @param  x_position_signal   Shared variable for passing in the ball's X position on the platform.
        @param  y_position_signal   Shared variable for passing in the ball's Y position on the platform.
        @param  ball_detected       Shared variable for indicating if the ball is on the touch panel.
        @param  euler_angles        Shared variable for passing in the platform's orientation.
        @param  print_queue         Shared variable for passing out print statements. 
    '''

    state = S0_WAIT

    # Start timestamp system
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)

    data_array = None
    write_index = 0

    filename = None
    array_len = 100


    while True:
        current_time = ticks_us()

        # Check if its time to run
        if ticks_diff(current_time, next_time) >= 0:

            next_time = ticks_add(next_time, period)

            if state == S0_WAIT:
                
                if data_collect_flag.read():
                    state = S1_COLLECT

                yield state

            if state == S1_COLLECT:
                # Collects 1000 data points before saving the data file


                if filename is None:
                    now = time.localtime()
                    filename = f"data_file_{now[3]}-{now[4]}-{now[5]}.txt"
                    start_time = current_time

                    # Data array will be 1000 rows x (Time, X_Pos, Y_Pos, Ball_Detect, IMU_Roll, IMU_Pitch)
                    data_array = [0, 0, 0, 0, 0, 0]
                    data_array[0] = array.array('f', array_len*[0.0])
                    data_array[1] = array.array('f', array_len*[0.0])
                    data_array[2] = array.array('f', array_len*[0.0])
                    data_array[3] = array.array('f', array_len*[0.0])
                    data_array[4] = array.array('f', array_len*[0.0])
                    data_array[5] = array.array('f', array_len*[0.0])

                    write_index = 0
                    print_queue.quick_print("Started Data Collection!")

                if not data_collect_flag.read() and filename is not None:
                    # Need to wrap up data collection
                    # print(data_array)
                    with open(filename, 'w') as f:
                        f.write("Time [ticks], X_Pos, Y_Pos, Ball Detected, EUL Roll, EUL Pitch\n")
                        for val in range(write_index):
                            f.write(
                                f"{data_array[0][val]} {data_array[1][val]} {data_array[2][val]} {data_array[3][val]} {data_array[4][val]}\n")

                    data_array = None
                    write_index = 0
                    filename = None
                    print_queue.quick_print("Finished Data Collection")
                    state = S0_WAIT


                else:
                    # Time
                    data_array[0][write_index] = ticks_diff(current_time, start_time) 
                    # X Position
                    x_pos = x_position_signal.read()
                    # print(x_pos)
                    data_array[1][write_index] = x_pos
                    # Y Position
                    y_pos = y_position_signal.read()
                    # print(y_pos)
                    data_array[2][write_index] = y_pos
                    # Ball Detected
                    ball_det = ball_detected.read()
                    # print(ball_det)
                    data_array[3][write_index] = ball_det

                    eul = euler_angles.read()
                    # print(eul)

                    # EUL Roll
                    data_array[4][write_index] = eul["EUL_ROLL"]
                    # EUL Pitch
                    data_array[5][write_index] = eul["EUL_PITCH"]

                    write_index += 1

                    if write_index >= array_len:
                        data_collect_flag.write(False)

                # print("Ran Rask Data")

                yield state

        else:
            yield

# if __name__ == '__main__':
#     roll_target_duty = shares.Share(50)
#     pitch_target_duty = shares.Share(25)

#     sample_period = 10000

#     task_list = [taskMotorFcn(taskName = 'Motor Task', period = 10_000, roll_target_duty = roll_target_duty, pitch_target_duty = pitch_target_duty)]

#     while True:
#         try:
#             for task in task_list:
#                 next(task)
#         except KeyboardInterrupt:
#             break

#     print("Terminating Program")
