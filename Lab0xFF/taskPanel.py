'''!@file       taskPanel.py
    @brief      Task for creating, calibrating, and operating the touch panel object.
    @details    If no calibration file exists, this task first runs the calibration method. The coefficients are then stored in a text file on the 
                pyBoard for later use. If the file already exists, the task skips the calibration process and uses the values from the existing file.
                This progression can be seen in the state diagram below. <br>
                
                <img src="Resources/Lab5taskPanelState.png" alt="Lab 5 State Diagram" width="600"><br/>
                
    @author     Caleb Savard
    @author     Chris Linthacum
    @date       March 17, 2022

'''

import shares
from time import ticks_us, ticks_diff, ticks_add
from touchPanel import TouchPanel
import pyb, micropython
from printqueue import PrintQueue, PrintJob
import os
from ulab import numpy as np


##  @brief      State 0
#   @details    Const() value for the read state
S0_READ = micropython.const(0)
##  @brief      State 1
#   @details    Cosnt() value for the calibration state
S1_CALIB = micropython.const(1)

def taskPanelFcn(taskName: str,
                    period: int,
                    x_position_signal: shares.Share,
                    y_position_signal: shares.Share,
                    ball_detected: shares.Share,
                    panelCalStatus: shares.Share,
                    print_queue: PrintQueue,
                    touch_calib_status: shares.Share,
                    imu_calib_status: shares.Share,
                    x_pos_deriv_signal: shares.Share,
                    y_pos_deriv_signal: shares.Share):
    '''!@brief                  Continuously reads touch panel position
        @details                Each run, updates shared variables with touch panel ball position
        @param  taskName        Task name label for debugging purposes
        @param  period          Period to run taskPanelFcn
        @param  x_position_signal   Shared variable to store x position of the ball
        @param  y_position_signal   Shared variable to story y position of the ball
        @param  ball_detected       Shared variable to store boolean of whether or not the ball is detected
        @param  panelCalStatus      Shared variable to store whether or not the panel is calibrated.
        @param  print_queue         Print Queue object for passing print messages to the taskUser.
        @param  touch_calib_status  Shared variable for whether the touch calibration status is true or false.
        @param  imu_calib_status    Shared variable for whether or not the IMU has been calibrated. (Will not attempt to calibrate touch panel until IMU is complete.)
        @param  x_pos_deriv_signal  Shared variable to store x position derivative
        @param  y_pos_deriv_signal  Shared variable to store y position derivative
    '''

    ## Initialize the TouchPanel object
    panel = TouchPanel(pyb.Pin.cpu.A0, pyb.Pin.cpu.A6, pyb.Pin.cpu.A1, pyb.Pin.cpu.A7, print_queue=print_queue)

    state = 0
    calib_fcn = None

    ## Check if calibration constants exist, if not, enter calibration state
    files = os.listdir()
    if "Panel_Cal_Coeffs.txt" in files:
        print_queue.add_job(PrintJob("Panel Calibration file found."))
        with open("Panel_Cal_Coeffs.txt", 'r') as f:

            cal_data_string = f.read()
            print(cal_data_string)
           
            cal_values = []    
            for val in cal_data_string.strip().split(', '):
                try:
                    cal_values.append(float(val))
                except ValueError:
                    print(f"Error raised on '{val}'")
                    raise ValueError
            # cal_values = [float(val) for val in cal_data_string.strip().split(',')]
            # cal_values = [[cal_values[0], cal_values[1]],
            #               [cal_values[2], cal_values[3]],
            #               [cal_values[4], cal_values[5]]]

            cal_values_arr = np.zeros((3, 2))
            
            cal_values_arr[0][0] = cal_values[0]
            cal_values_arr[0, 1] = cal_values[1]
            cal_values_arr[1, 0] = cal_values[2]
            cal_values_arr[1, 1] = cal_values[3]
            cal_values_arr[2, 0] = cal_values[4]
            cal_values_arr[2, 1] = cal_values[5]
            
            
            panel.calibCoeffs = cal_values_arr

            panelCalStatus.write(True)
            print_queue.add_job(PrintJob("Loaded Panel Calibration Values"))
            touch_calib_status.write(True)

            state = S0_READ
    else:
        # print_queue.add_job(PrintJob("No Panel Calibration Found. Starting Calibration."))
        touch_calib_status.write(False)
        state = S1_CALIB
        
    next_time = ticks_add(ticks_us(), period)
    last_time = 0    

    while True:
        current_time = ticks_us()
        
        if ticks_diff(current_time, next_time) >= 0:

            next_time = ticks_add(next_time, period)

            if state == S0_READ:
                vals = panel.scan()
                if vals[2]:
                    
                    x_pos_deriv_signal.write((x_position_signal.read() - vals[0]) / (ticks_diff(current_time, last_time) * (10 ** -6)))
                    y_pos_deriv_signal.write((y_position_signal.read() - vals[1]) / (ticks_diff(current_time, last_time) * (10 ** -6)))

                    last_time = current_time
                    
                    x_position_signal.write(vals[0])
                    y_position_signal.write(vals[1])



                    ball_detected.write(True)
                else:
                    ball_detected.write(False)
                                
            elif state == S1_CALIB:

                # Wait for IMU Calibration to finish before starting touch panel calibration
                while (not imu_calib_status.read() or not imu_calib_status.readData()):
                    yield state
                
                if calib_fcn is None:
                    calib_fcn = panel.calibrate(touch_calib_status=touch_calib_status)

                if not touch_calib_status.read():
                    # print(touch_calib_status.read())
                    try:
                        next(calib_fcn)
                    except StopIteration:
                        # print("Caught StopIteration")
                        pass
                    
                    yield state
                else:
                    state = S0_READ
                

            else:
                raise ValueError("Invalid State in taskPanel")

            yield state
                

    
